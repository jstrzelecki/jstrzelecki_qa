import random
import string


def generate_random_name(length):
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for _ in range(length))


# if __name__ == '__main__':
#     generate_random_name(10)
