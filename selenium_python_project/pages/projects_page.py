from selenium.webdriver.common.by import By


class ProjectPage:
    add_project_selector = (By.CLASS_NAME, 'button_link')
    projects_button_selector = (
        By.CSS_SELECTOR, 'a.activeMenu[href="http://demo.testarena.pl/administration/projects"]')
    name_field = (By.CSS_SELECTOR, "input#name")
    prefix_field = (By.CSS_SELECTOR, "input#prefix")
    save_button = (By.CSS_SELECTOR, "input#save")
    search_field_selector = (By.CSS_SELECTOR, "input#search")
    search_button_selector = (By.CSS_SELECTOR, "a#j_searchButton")

    def __init__(self, browser):
        self.browser = browser

    def click_add_project_button(self):
        self.browser.find_element(*self.add_project_selector).click()
        return self

    def add_project(self, name, prefix):
        self.browser.find_element(*self.name_field).send_keys(name)
        self.browser.find_element(*self.prefix_field).send_keys(prefix)
        self.browser.find_element(*self.save_button).click()
        return self

    def click_projects_button(self):
        self.browser.find_element(*self.projects_button_selector).click()
        return self

    def search_added_project(self, name):
        self.browser.find_element(*self.search_field_selector).send_keys(name)
        self.browser.find_element(*self.search_button_selector).click()
        self.browser.find_element(*self.search_button_selector).click()
        return self
