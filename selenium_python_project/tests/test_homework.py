from selenium.webdriver.common.by import By
from selenium_python_project.fixtures.testarena.login import browser
from selenium_python_project.fixtures.chrome import chrome_browser
from selenium_python_project.utils.helpers import generate_random_name
from selenium_python_project.pages.home_page import HomePage
from selenium_python_project.pages.projects_page import ProjectPage

administrator_email = 'administrator@testarena.pl'


def test_successful_login(browser):
    home_page = HomePage(browser)
    user_email = home_page.get_current_user_email()
    assert administrator_email == user_email


def test_add_and_search_new_project(browser):
    name = generate_random_name(10)
    prefix = generate_random_name(3)

    home_page = HomePage(browser)
    home_page.open_admin_panel()
    project_page = ProjectPage(browser)
    project_page.click_add_project_button()
    project_page.add_project(name, prefix)
    project_page.click_projects_button()
    project_page.search_added_project(name)
    found_project_name_selector = browser.find_element(By.LINK_TEXT, name)
    found_project_name = found_project_name_selector.text
    assert found_project_name == name
