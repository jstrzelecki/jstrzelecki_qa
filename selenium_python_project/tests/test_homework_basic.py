from selenium_python_project.utils.helpers import generate_random_name
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_adding_new_project():
    administrator_email = 'administrator@testarena.pl'
    administration_panel_selector = ('div.header_admin > a[href="http://demo.testarena.pl/administration"]['
                                     'title="Administracja"]')
    projects_button_selector = 'a.activeMenu[href="http://demo.testarena.pl/administration/projects"]'
    project_name = generate_random_name(8)
    prefix_string = generate_random_name(3)

    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.maximize_window()
    browser.get('http://demo.testarena.pl/zaloguj')

    browser.find_element(By.CSS_SELECTOR, "#email").send_keys(administrator_email)
    browser.find_element(By.CSS_SELECTOR, "#password").send_keys("sumXQQ72$L")
    browser.find_element(By.CSS_SELECTOR, "#login").click()
    browser.find_element(By.CSS_SELECTOR, administration_panel_selector).click()
    browser.find_element(By.CSS_SELECTOR, "a.button_link").click()
    browser.find_element(By.CSS_SELECTOR, "input#name").send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, "input#prefix").send_keys(prefix_string)
    browser.find_element(By.CSS_SELECTOR, "input#save").click()
    browser.find_element(By.CSS_SELECTOR, projects_button_selector).click()
    browser.find_element(By.CSS_SELECTOR, "input#search").send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, "a#j_searchButton").click()
    link_element = browser.find_element(By.LINK_TEXT, project_name)
    link_title = link_element.text
    assert link_title == project_name
    browser.quit()
