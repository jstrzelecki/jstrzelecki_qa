import pytest
import requests
from base64 import b64encode
import lorem
import time

"""1. Dodaj nowy artykuł jako użytkownik editor
ZWERYFIKUJ: Powodzenie operacji CREATE
ZWERYFIKUJ: Autorstwo artykułu
2. Do artykułu dodanego w kroku 1 dodaj komentarz jako commenter
ZWERYFIKUJ: Powodzenie operacji CREATE
ZWERYFIKUJ: Relację między postem a komentarzem
ZWERYFIKUJ: Autorstwo komentarza
3. Do komentarza dodanego w kroku 2 dodaj odpowiedź jako editor
ZWERYFIKUJ: Powodzenie operacji CREATE
ZWERYFIKUJ: Relację pomiędzy komentarzem a odpowiedzią
ZWERYFIKUJ: Relację pomiędzy odpowiedzią a artykułem
ZWERYFIKUJ: Autorstwo odpowiedzi
"""

# GLOBAL VARIABLES

blog_url = 'https://gaworski.net'
editor_name = "John Editor"
editor_username = 'editor'
editor_password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
editor_token = b64encode(f"{editor_username}:{editor_password}".encode('utf-8')).decode("ascii")
commenter_name = "Albert Commenter"
commenter_username = 'commenter'
commenter_password = 'SXlx hpon SR7k issV W2in zdTb'
commenter_token = b64encode(f"{commenter_username}:{commenter_password}".encode('utf-8')).decode("ascii")

posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
users_endpoint_url = blog_url + "/wp-json/wp/v2/users"
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"


@pytest.fixture(scope='module')
def editor_headers():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + editor_token
    }
    return headers


@pytest.fixture(scope='module')
def commenter_headers():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + commenter_token
    }
    return headers


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post " + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph(),
    }
    return article


@pytest.fixture(scope='module')
def posted_article(article, editor_headers):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish"
    }
    response = requests.post(url=posts_endpoint_url, headers=editor_headers, json=payload)
    return response


@pytest.fixture(scope='module')
def comment(posted_article):
    timestamp = int(time.time())
    comment = {
        "comment_creation_date": timestamp,
        "post_id": posted_article.json()["id"],
        "comment_content": lorem.paragraph(),
    }
    return comment


@pytest.fixture(scope='module')
def posted_comment(comment, commenter_headers):
    payload = {
        "content": comment["comment_content"],
        "post": comment["post_id"],
        "status": "publish"
    }
    response = requests.post(url=comments_endpoint_url, headers=commenter_headers, json=payload)
    return response


@pytest.fixture(scope='module')
def reply_comment_to_posted_comment(posted_comment):
    timestamp = int(time.time())
    reply_comment = {
        "reply_comment_creation_date": timestamp,
        "initial_comment_id": posted_comment.json()["post"],
        "parent_post_id": posted_comment.json()["id"],
        "reply_content": lorem.paragraph(),
    }
    return reply_comment


@pytest.fixture(scope='module')
def posted_reply_to_comment(reply_comment_to_posted_comment, editor_headers):
    payload = {
        "content": reply_comment_to_posted_comment["reply_content"],
        "post": reply_comment_to_posted_comment["initial_comment_id"],
        "parent": reply_comment_to_posted_comment["parent_post_id"],
        "status": "publish"
    }
    response = requests.post(url=comments_endpoint_url, headers=editor_headers, json=payload)
    return response


def test_new_post_is_successfully_created(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"


def test_newly_created_post_has_correct_author(article, posted_article):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    published_article = requests.get(url=wordpress_post_url)
    assert published_article.status_code == 200
    assert published_article.reason == "OK"
    wordpress_post_data = published_article.json()
    author_id = wordpress_post_data["author"]
    editor_url = f"{users_endpoint_url}/{author_id}"
    get_post = requests.get(url=editor_url)
    assert get_post.json()["name"] == editor_name


def test_add_comment_to_new_post(comment, posted_comment, posted_article):
    assert posted_comment.status_code == 201
    assert posted_comment.reason == "Created"
    comment_id = posted_comment.json()["id"]
    commenter_url = f"{comments_endpoint_url}/{comment_id}"
    get_comment = requests.get(url=commenter_url)
    post_id = posted_article.json()["id"]
    assert posted_comment.json()["post"] == post_id
    assert get_comment.json()["author_name"] == commenter_name


def test_add_reply_to_comment(posted_reply_to_comment, posted_comment, posted_article):
    assert posted_reply_to_comment.status_code == 201
    assert posted_reply_to_comment.reason == "Created"
    reply_comment_id = posted_reply_to_comment.json()["id"]
    reply_comment_url = f"{comments_endpoint_url}/{reply_comment_id}"
    get_reply_comment = requests.get(url=reply_comment_url)
    post_id = posted_article.json()["id"]
    posted_comment_id = posted_reply_to_comment.json()["parent"]
    assert posted_reply_to_comment.json()["parent"] == posted_comment_id
    assert posted_reply_to_comment.json()["post"] == post_id
    assert get_reply_comment.json()["author_name"] == editor_name
