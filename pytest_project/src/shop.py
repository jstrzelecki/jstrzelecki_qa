class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        # total_price = 0
        # for product in self.products:
        #     total_price = total_price + product.get_price()
        # return total_price
        return sum([product.get_price() for product in self.products])

    def get_total_quantity_of_products(self):
        # total_quantity = 0
        # for product in self.products:
        #     total_quantity = total_quantity + product.quantity
        # return total_quantity
        return sum([product.quantity for product in self.products])

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


if __name__ == '__main__':
    test_order = Order('jacek_strzelecki@example.com')
    sneakers = Product('Sneakers', 70.00, 9)
    bag = Product('Bag', 20.00, 1)
    jumper = Product('Jumper', 20.00)
    test_order.add_product(sneakers)
    test_order.add_product(jumper)
    test_order.add_product(bag)
    print(test_order.get_total_price())
    print(test_order.get_total_quantity_of_products())
    print(sneakers.get_price())
